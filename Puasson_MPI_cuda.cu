// This is the explicit conjugate gradient method for descrete Puasson problem
// on uniform mesh.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

#define DebugOutput 0
#define Print
#define Step 10
#define PrintStep 1
#define DoOutputRes 1

#define MIN_ERR 1e-4

#define TRUE  ((int) 1)
#define FALSE ((int) 0)

#define TAG_RES     40
#define TAG_BAS     41
#define TAG_SOLV    42

#define Max(A,B) ((A)>(B)?(A):(B))
#define Min(A,B) ((A)<(B)?(A):(B))
#define R2(x,y) ((x)*(x)+(y)*(y))
#define Cube(x) ((x)*(x)*(x))
#define xpos(i,h0) ((i)*(h0))
#define ypos(j,h1) ((j)*(h1))

#define LeftPart(P,i,j,N0,h0,h1)\
    ((-(P[(N0)*(j) + (i) + 1] - P[(N0)*(j) + (i)]) / (h0) + (P[(N0)*(j) + (i)] - P[(N0)*(j) + (i) - 1]) / (h0)) / (h0) + \
    (-(P[(N0)*((j) + 1) + (i)] - P[(N0)*(j) + (i)]) / (h1) + (P[(N0)*(j) + (i)] - P[(N0)*((j) - 1) + (i)]) / (h1)) / (h1))

#define Solution(x, y) 1 + sin((x)*(y))
#define BoundaryValue(x, y) 1 + sin((x)*(y))

#define WARP_SIZE 32
#define BLOCK_SZ_X 16
#define BLOCK_SZ_Y 16

#define CUDA_CHECK_ERR(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) {
      printf("GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

void printDevProp(cudaDeviceProp devProp)
{
    printf("%s\n", devProp.name);
    printf("Major revision number:         %d\n", devProp.major);
    printf("Minor revision number:         %d\n", devProp.minor);
    printf("Total global memory:           %zu", devProp.totalGlobalMem);
    printf(" bytes\n");
    printf("Number of multiprocessors:     %d\n", devProp.multiProcessorCount);
    printf("Total amount of shared memory per block: %zu\n",devProp.sharedMemPerBlock);
    printf("Total registers per block:     %d\n", devProp.regsPerBlock);
    printf("Warp size:                     %d\n", devProp.warpSize);
    printf("Maximum memory pitch:          %zu\n", devProp.memPitch);
    printf("Total amount of constant memory:         %zu\n",   devProp.totalConstMem);
    return;
}


__global__
void cudaNewBasisKernel(double *resVectD, double *BasisVectD, double alpha,
                int stride, int patchSizeX, int patchSizeY)
{
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    if(x < patchSizeX && y < patchSizeY) {
        int idx = stride * (y + 1) + (x + 1);
        // main operation
        BasisVectD[idx] = resVectD[idx] - alpha * BasisVectD[idx];
    }
}

__global__
void cudaNewResidualKernel(double *resVectD, double *SolVectD, double *RHS_VectD,
                int stride, double h0, double h1, int patchSizeX, int patchSizeY)
{
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    if(x < patchSizeX && y < patchSizeY) {
        int idx = stride * (y + 1) + (x + 1);
        // main operation
        resVectD[idx] = LeftPart(SolVectD, x + 1, y + 1, stride, h0, h1) - RHS_VectD[idx];
    }
}

__global__
void productArrKernel(double *arrFirstD, double *arrSecondD, double *outBlocksD,
                int stride, double h0, double h1, int patchSizeX, int patchSizeY)
{
    __shared__ double data[BLOCK_SZ_X * BLOCK_SZ_Y];
    int tid_x = threadIdx.x;
    int tid_y = threadIdx.y;
    int tid = tid_y * blockDim.x + tid_x;

    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    if(x < patchSizeX && y < patchSizeY) {
        int idx = stride * (y + 1) + x + 1;
        data[tid] = LeftPart(arrFirstD, x + 1, y + 1, stride, h0, h1) * arrSecondD[idx] * h0 * h1;
    } else {
        data[tid] = 0;
    }
    __syncthreads();

    // reduce
    for(unsigned int s = blockDim.x * blockDim.y / 2; s > 0; s>>=1) {
        if(tid < s) {
            data[tid] += data[tid + s];
        }
        __syncthreads();
    }
    if (tid == 0) {
        int outIdx = gridDim.x * blockIdx.y + blockIdx.x;
        outBlocksD[outIdx] = data[0];
    }
}

__global__
void productRGKernel(double *ResVectD, double *BasisVectD, double *outBlocksD,
                int stride, double h0xh1, int patchSizeX, int patchSizeY)
{
    __shared__ double data[BLOCK_SZ_X * BLOCK_SZ_Y];
    int tid = threadIdx.y * blockDim.x + threadIdx.x;
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    if(x < patchSizeX && y < patchSizeY) {
        int idx = stride * (y + 1) + x + 1;
        data[tid] = ResVectD[idx] * BasisVectD[idx] * h0xh1;
    } else {
        data[tid] = 0;
    }
    __syncthreads();

    // sum reduce
    for(unsigned int s = blockDim.x * blockDim.y / 2; s > 0; s>>=1) {
        if(tid < s) {
            data[tid] += data[tid + s];
        }
        __syncthreads();
    }
    if (tid == 0) {
        int outIdx = gridDim.x * blockIdx.y + blockIdx.x;
        outBlocksD[outIdx] = data[0];
    }
}

__global__
void updateSolutionAndGetErrorKernel(double *SolVectD, double *BasisVectD, double *outBlocksD, double tau,
                int st0, int st1, int stride, double h0, double h1, int patchSizeX, int patchSizeY)
{
    __shared__ double err_delta[BLOCK_SZ_X * BLOCK_SZ_Y];
    __shared__ double err_real[BLOCK_SZ_X * BLOCK_SZ_Y];

    int tid = threadIdx.y * blockDim.x + threadIdx.x;
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    if(x < patchSizeX && y < patchSizeY) {
        int x_glob = st0 + x;
        int y_glob = st1 + y;
        int idx = stride * (y + 1) + x + 1;
        // calculate new value
        double oldValue = SolVectD[idx];
        double newValue = oldValue - tau * BasisVectD[idx];
        double realValue = Solution(xpos(x_glob, h0), ypos(y_glob, h1));
        // store error
        err_delta[tid] = fabs(newValue - oldValue);
        err_real[tid] = fabs(newValue - realValue);
        // update solution
        SolVectD[idx] = newValue;
    } else {
        err_delta[tid] = 0;
        err_real[tid] = 0;
    }
    __syncthreads();

    // reduce error
    for(unsigned int s = blockDim.x * blockDim.y / 2; s > 0; s>>=1) {
        if(tid < s) {
            err_delta[tid] = Max(err_delta[tid], err_delta[tid + s]);
            err_real[tid] = Max(err_real[tid], err_real[tid + s]);
        }
        __syncthreads();
    }
    if (tid == 0) {
        int outIdx = gridDim.x * blockIdx.y + blockIdx.x;
        outBlocksD[outIdx] = err_delta[0];
        outBlocksD[outIdx + gridDim.y*gridDim.x] = err_real[0];
    }
}

int RightPart(double * rhs, int st0, int st1, double h0, double h1, int patchSzXExt, int patchSzYExt)
{
    int i, j;
    double x, y;

    for (j = 0; j < patchSzYExt; j++) {
        y = ypos(j + st1 - 1, h1);
        for(i = 0; i < patchSzXExt; i++) {
            x = xpos(i + st0 - 1, h0);
            rhs[j*patchSzXExt + i] = R2(x, y) * sin(x*y);
        }
    }

    return 0;
}

int IsPower(int Number)
// the function returns log_{2}(Number) if it is integer. If not it returns (-1).
{
    unsigned int M;
    int p;

    if (Number <= 0) {
        return(-1);
    }

    M = Number; p = 0;
    while (M % 2 == 0) {
        ++p;
        M = M >> 1;
    }
    if ((M >> 1) != 0) {
        return(-1);
    }
    else {
        return(p);
    }

}

int SplitFunction(int N0, int N1, int p)
// This is the splitting procedure of proc. number p. The integer p0
// is calculated such that abs(N0/p0 - N1/(p-p0)) --> min.
{
    float n0, n1;
    int p0, i;

    n0 = (float)N0; n1 = (float)N1;
    p0 = 0;

    for (i = 0; i < p; i++) {
        if (n0 > n1) {
            n0 = n0 / 2.0;
            ++p0;
        }
        else {
            n1 = n1 / 2.0;
        }
    }
    return(p0);
}

void Communicate(MPI_Comm Grid_Comm,
    double *arrayD, int TAG_COMM,
    double *sendBuff0, double *recvBuff0,
    double *sendBuff1, double *recvBuff1,
    int patchSzXExt,
    int patchSzYExt,
    int up, int down, int left, int right)
{

    MPI_Status status;

    /* ---------------------------------------------- */
    /* ----- send to top and receive from bottom ---- */
    /* ---------------------------------------------- */
    if (up >= 0) {
        // first line
        // get from gpu
        int offsetFirstLine = patchSzXExt*1 + 1;
        CUDA_CHECK_ERR(cudaMemcpy(sendBuff0, arrayD + offsetFirstLine,
            sizeof(double)*(patchSzXExt - 2), cudaMemcpyDeviceToHost));
    }
    if (up >= 0 && down >= 0) {
        // send and receive
        MPI_Sendrecv(sendBuff0, patchSzXExt - 2, MPI_DOUBLE, up, TAG_COMM,
            recvBuff0, patchSzXExt - 2, MPI_DOUBLE, down, TAG_COMM, Grid_Comm, &status);
    }
    else if (up >= 0) {
        // only send
        MPI_Send(sendBuff0, patchSzXExt - 2, MPI_DOUBLE, up, TAG_COMM, Grid_Comm);
    }
    else if (down >= 0) {
        // only receive
        MPI_Recv(recvBuff0, patchSzXExt - 2, MPI_DOUBLE, down, TAG_COMM, Grid_Comm, &status);
    }
    if (down >= 0) {
        // update data (line after bottom)
        // send to gpu
        int offsetLineAfter = patchSzXExt*(patchSzYExt - 1) + 1;
        CUDA_CHECK_ERR(cudaMemcpy(arrayD + offsetLineAfter, recvBuff0,
            sizeof(double)*(patchSzXExt - 2), cudaMemcpyHostToDevice));
    }

    /* ---------------------------------------------- */
    /* ----- send to bottom and receive from top ---- */
    /* ---------------------------------------------- */
    if (down >= 0) {
        // last line
        // get from gpu
        int offsetLastLine = patchSzXExt*(patchSzYExt - 2) + 1;
        CUDA_CHECK_ERR(cudaMemcpy(sendBuff0, arrayD + offsetLastLine,
            sizeof(double)*(patchSzXExt - 2), cudaMemcpyDeviceToHost));
    }
    if (up >= 0 && down >= 0) {
        // send and receive
        MPI_Sendrecv(sendBuff0, patchSzXExt - 2, MPI_DOUBLE, down, TAG_COMM,
            recvBuff0, patchSzXExt - 2, MPI_DOUBLE, up, TAG_COMM, Grid_Comm, &status);
    }
    else if (down >= 0) {
        // only send
        MPI_Send(sendBuff0, patchSzXExt - 2, MPI_DOUBLE, down, TAG_COMM, Grid_Comm);
    }
    else if (up >= 0) {
        // only receive
        MPI_Recv(recvBuff0, patchSzXExt - 2, MPI_DOUBLE, up, TAG_COMM, Grid_Comm, &status);
    }
    if (up >= 0) {
        // update data (line before top)
        // send to gpu
        int offsetLineBefore = patchSzXExt*0 + 1;
        CUDA_CHECK_ERR(cudaMemcpy(arrayD + offsetLineBefore, recvBuff0,
            sizeof(double)*(patchSzXExt - 2), cudaMemcpyHostToDevice));
    }

    /* ---------------------------------------------- */
    /* ----- send to right and receive from left ---- */
    /* ---------------------------------------------- */
    // prepare data for sending
    if (right >= 0) {
        // right (last) column
        // get from gpu
        int offsetLastColumn = patchSzXExt*1 + patchSzXExt - 2;
        int dpitch = 1*sizeof(double);
        int spitch = patchSzXExt * sizeof(double);
        CUDA_CHECK_ERR(cudaMemcpy2D(sendBuff1, dpitch, arrayD + offsetLastColumn,
            spitch, 1*sizeof(double), patchSzYExt - 2, cudaMemcpyDeviceToHost));

    }
    if (left >= 0 && right >= 0) {
        // send and receive
        MPI_Sendrecv(sendBuff1, patchSzYExt - 2, MPI_DOUBLE, right, TAG_COMM,
            recvBuff1, patchSzYExt - 2, MPI_DOUBLE, left, TAG_COMM, Grid_Comm, &status);
    }
    else if (right >= 0) {
        // only send
        MPI_Send(sendBuff1, patchSzYExt - 2, MPI_DOUBLE, right, TAG_COMM, Grid_Comm);
    }
    else if (left >= 0) {
        // only receive
        MPI_Recv(recvBuff1, patchSzYExt - 2, MPI_DOUBLE, left, TAG_COMM, Grid_Comm, &status);
    }
    if (left >= 0) {
        // update data (column before left)
        // send to gpu
        int offsetColumnBefore = patchSzXExt*1 + 0;
        int dpitch = patchSzXExt * sizeof(double);
        int spitch = 1*sizeof(double);
        CUDA_CHECK_ERR(cudaMemcpy2D(arrayD + offsetColumnBefore, dpitch, recvBuff1,
            spitch, 1*sizeof(double), patchSzYExt - 2, cudaMemcpyHostToDevice));
    }


    /* ---------------------------------------------- */
    /* ----- send to left and receive from right ---- */
    /* ---------------------------------------------- */
    // prepare data for sending
    if (left >= 0) {
        // left (first) column
        // get from gpu
        int offsetFirstColumn = patchSzXExt * 1 + 1;
        int dpitch = 1 * sizeof(double);
        int spitch = patchSzXExt * sizeof(double);
        CUDA_CHECK_ERR(cudaMemcpy2D(sendBuff1, dpitch, arrayD + offsetFirstColumn,
            spitch, 1*sizeof(double), patchSzYExt - 2, cudaMemcpyDeviceToHost));
    }
    if (left >= 0 && right >= 0) {
        // send and receive
        MPI_Sendrecv(sendBuff1, patchSzYExt - 2, MPI_DOUBLE, left, TAG_COMM,
            recvBuff1, patchSzYExt - 2, MPI_DOUBLE, right, TAG_COMM, Grid_Comm, &status);
    }
    else if (left >= 0) {
        // only send
        MPI_Send(sendBuff1, patchSzYExt - 2, MPI_DOUBLE, left, TAG_COMM, Grid_Comm);
    }
    else if (right >= 0) {
        // only receive
        MPI_Recv(recvBuff1, patchSzYExt - 2, MPI_DOUBLE, right, TAG_COMM, Grid_Comm, &status);
    }
    if (right >= 0) {
        // update data (column after right)
        // send to gpu
        int offsetColumnAfter = patchSzXExt*1 + patchSzXExt  - 1;
        int dpitch = patchSzXExt * sizeof(double);
        int spitch = 1*sizeof(double);
        CUDA_CHECK_ERR(cudaMemcpy2D(arrayD + offsetColumnAfter, dpitch, recvBuff1,
            spitch, 1*sizeof(double), patchSzYExt - 2, cudaMemcpyHostToDevice));
    }

    return;
}

int main(int argc, char * argv[])
{

    // Domain size.
    const double A = 2.0;
    const double B = 2.0;

    int N0, N1;                             // the number of internal points on axes (ox) and (oy).
    double h0, h1;                          // mesh steps on (0x) and (0y) axes
    int n0, n1, k0, k1;                     // N0 = n0*dims[0] + k0, N1 = n1*dims[1] + k1.
    int patchSzX, patchSzY;                         // subregion size
    int st0, st1;                           // subregion start index
    int en0, en1;                           // subregion end index (not including)
    bool is_edge_st0, is_edge_st1;
    bool is_edge_en0, is_edge_en1;

    int ProcNum, rank;                      // the number of processes and rank in communicator.
    int power, p0, p1;                      // ProcNum = 2^(power), power splits into sum p0 + p1.
    int dims[2];                            // dims[0] = 2^p0, dims[1] = 2^p1 (--> M = dims[0]*dims[1]).

    int Coords[2];                          // the process coordinates in the cartesian topology created for mesh.
    MPI_Comm Grid_Comm;                     // this is a handler of a new communicator.
    const int ndims = 2;                    // the number of a process topology dimensions.
    int periods[2] = { 0, 0 };              // it is used for creating processes topology.
    int left, right, up, down;              // the neighbours of the process.

    double *SolVect;                        // the solution array
    double *ResVect;                        // the residual array
    double *RHS_Vect;                       // the right hand side of Puasson equation.

    // same on device
    double *SolVectD;
    double *ResVectD;
    double *BasisVectD;
    double *RHS_VectD;

    double sp, alpha, tau, NewValue, err;
    double alpha_local, sp_local, tau_local;
    double err_local;
    double real_err_local;
    double real_err;
    double compErr;

    int SDINum, CGMNum;                     // the number of steep descent and CGM iterations.
    int counterSDI;                         // the current iteration number.
    int counterCGM;                         // the current iteration number.

    double *sendBuff0;                      // buffer for sending data on axis 0
    double *recvBuff0;                      // buffer for receiving data on axis 0
    double *sendBuff1;                      // same for axis 1
    double *recvBuff1;                      // for axis 1

    int i, j;
    char str[127];
    FILE * fp = NULL;
    FILE * fp_iter = NULL;

    double startTime, endTime, totalTime, iterAvgTime;
    double iterStart, iterEnd, iterTime;

    // MPI Library is being activated ...
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // command line analizer
    switch (argc)
    {
    case 4: {
        SDINum = 1;
        CGMNum = atoi(argv[3]);
        break;
    }
    case 5: {
        SDINum = Max(atoi(argv[3]), 1);      // SDINum >= 1
        CGMNum = atoi(argv[4]);
        break;
    }
    default: {
        if (rank == 0)
            printf("Wrong number of parameters in command line.\nUsage: <ProgName> "
                "<Nodes number on (0x) axis> <Nodes number on (0y) axis> "
                "[the number of steep descent iterations] "
                "<the number of conjugate gragient iterations>\nFinishing...\n");
        MPI_Finalize();
        return(1);
    }
    }

    N0 = atoi(argv[1]); N1 = atoi(argv[2]);
    h0 = A / (N0 - 1);    h1 = B / (N1 - 1);

    if ((N0 <= 0) || (N1 <= 0)) {
        if (rank == 0)
            printf("The first and the second arguments (mesh numbers) should be positive.\n");

        MPI_Finalize();
        return(2);
    }

    if ((power = IsPower(ProcNum)) < 0) {
        if (rank == 0)
            printf("The number of procs must be a power of 2.\n");

        MPI_Finalize();
        return(3);
    }

    p0 = SplitFunction(N0, N1, power);
    p1 = power - p0;

    dims[0] = (unsigned int)1 << p0;   dims[1] = (unsigned int)1 << p1;
    n0 = N0 >> p0;                      n1 = N1 >> p1;
    k0 = N0 - dims[0] * n0;             k1 = N1 - dims[1] * n1;

    // the cartesian topology of processes is being created ...
    MPI_Cart_create(MPI_COMM_WORLD, ndims, dims, periods, TRUE, &Grid_Comm);
    MPI_Comm_rank(Grid_Comm, &rank);
    MPI_Cart_coords(Grid_Comm, rank, ndims, Coords);

    MPI_Cart_shift(Grid_Comm, 0, 1, &left, &right);
    MPI_Cart_shift(Grid_Comm, 1, -1, &down, &up);

    // start coord
    st0 = Min(k0, Coords[0]) * (n0 + 1) + Max(0, (Coords[0] - k0)) * n0;
    st1 = Min(k1, Coords[1]) * (n1 + 1) + Max(0, (Coords[1] - k1)) * n1;

    // not including end coord (0...en0-1)
    en0 = st0 + n0 + (Coords[0] < k0 ? 1 : 0);
    en1 = st1 + n1 + (Coords[1] < k1 ? 1 : 0);

    // do not include boundary
    is_edge_st0 = st0 == 0;
    st0 = Max(1, st0);
    is_edge_st1 = st1 == 0;
    st1 = Max(1, st1);

    is_edge_en0 = (en0 == N0);
    en0 = Min(N0 - 1, en0);
    is_edge_en1 = (en1 == N1);
    en1 = Min(N1 - 1, en1);

    // patch active size
    patchSzX = en0 - st0;
    patchSzY = en1 - st1;

    // patch expanded size
    int patchSzXExt = patchSzX + 2;
    int patchSzYExt = patchSzY + 2;

    totalTime = 0;

    // init output file stream
    if (rank == 0) {
        sprintf(str, "Puasson_CUDA_ECGM_%dx%d_%d.log", N0, N1, ProcNum);
        fp = fopen(str, "w");
        fprintf(fp, "The Domain: [0,%f]x[0,%f], number of points: N[0,A] = %d, N[0,B] = %d;\n", A, B, N0, N1);
        printf("Number of processes: %d, using CUDA\n", ProcNum);
        printf("The Domain: [0,%f]x[0,%f], number of points: N[0,A] = %d, N[0,B] = %d;\n", A, B, N0, N1);

        sprintf(str, "IterNorm_CUDA_%dx%d_%d.log", N0, N1, ProcNum);
        fp_iter = fopen(str, "w");
    }

    // allocate memory on host, expanded for storing edge values
    CUDA_CHECK_ERR(cudaMallocHost((void**)&SolVect, patchSzXExt*patchSzYExt*sizeof(double)));
    CUDA_CHECK_ERR(cudaMallocHost((void**)&ResVect, patchSzXExt*patchSzYExt*sizeof(double)));
    CUDA_CHECK_ERR(cudaMallocHost((void**)&RHS_Vect, patchSzXExt*patchSzYExt*sizeof(double)));

    CUDA_CHECK_ERR(cudaMallocHost((void**)&sendBuff0, patchSzXExt*sizeof(double)));
    CUDA_CHECK_ERR(cudaMallocHost((void**)&recvBuff0, patchSzXExt*sizeof(double)));
    CUDA_CHECK_ERR(cudaMallocHost((void**)&sendBuff1, patchSzYExt*sizeof(double)));
    CUDA_CHECK_ERR(cudaMallocHost((void**)&recvBuff1, patchSzYExt*sizeof(double)));

    if (rank == 0) {
        printf("Computing boundary values...\n");
    }

    // initialization of arrays
    // right part
    CUDA_CHECK_ERR(cudaMemset(ResVect, 0, patchSzXExt*patchSzYExt*sizeof(double)));
    RightPart(RHS_Vect, st0, st1, h0, h1, patchSzXExt, patchSzYExt);

    // init boundary
    CUDA_CHECK_ERR(cudaMemset(SolVect, 0, patchSzXExt*patchSzYExt*sizeof(double)));
    if(is_edge_st1) {
        for (i = 0; i < patchSzXExt; i++) {
            SolVect[i] = BoundaryValue(xpos(i + st0 - 1, h0), 0.0);
        }
    }
    if(is_edge_en1) {
        for (i = 0; i < patchSzXExt; i++) {
            SolVect[patchSzXExt*(patchSzYExt - 1) + i] = BoundaryValue(xpos(i + st0 - 1, h0), B);
        }
    }
    if(is_edge_st0) {
        for (j = 0; j < patchSzYExt; j++) {
            SolVect[patchSzXExt*j] = BoundaryValue(0.0, ypos(j + st1 - 1, h1));
        }
    }
    if(is_edge_en0) {
        for (j = 0; j < patchSzYExt; j++) {
            SolVect[patchSzXExt*j + (patchSzXExt - 1)] = BoundaryValue(A, ypos(j + st1 - 1, h1));
        }
    }
    if (rank == 0) {
        printf("Boundary values computed.\n");
        printf("Running SDI iterations...\n");
    }

    // init CUDA
    CUDA_CHECK_ERR(cudaSetDevice(rank % 2));

    int cudaGridSizeX = (patchSzX + BLOCK_SZ_X - 1) / BLOCK_SZ_X;
    int cudaGridSizeY = (patchSzY + BLOCK_SZ_Y - 1) / BLOCK_SZ_Y;
    int cudaGridSizeFull = cudaGridSizeX * cudaGridSizeY;

    dim3 dimBlock(BLOCK_SZ_X, BLOCK_SZ_Y);
    dim3 dimGrid(cudaGridSizeX, cudaGridSizeY);

#if DebugOutput
    if(rank == 0) {
        printf("st0: %d, en0: %d, st1: %d, en1: %d\n", st0, en0, st1, en1);
        printf("patchSzX: %d, patchSzY: %d, patchSzXExt: %d, patchSzYExt: %d\n", patchSzX, patchSzY, patchSzXExt, patchSzYExt);
        printf("BLOCK_SZ_X: %d, BLOCK_SZ_Y: %d\n", BLOCK_SZ_X, BLOCK_SZ_Y);
        printf("cudaGridSizeX: %d, cudaGridSizeY: %d\n", cudaGridSizeX, cudaGridSizeY);

        cudaDeviceProp devProp;
        cudaGetDeviceProperties(&devProp, 0);
        printDevProp(devProp);
    }

#endif

    // allocate memory on device
    CUDA_CHECK_ERR(cudaMalloc((void**)&SolVectD, patchSzXExt*patchSzYExt*sizeof(double)));
    CUDA_CHECK_ERR(cudaMalloc((void**)&BasisVectD, patchSzXExt*patchSzYExt*sizeof(double)));
    CUDA_CHECK_ERR(cudaMalloc((void**)&ResVectD, patchSzXExt*patchSzYExt*sizeof(double)));
    CUDA_CHECK_ERR(cudaMalloc((void**)&RHS_VectD, patchSzXExt*patchSzYExt*sizeof(double)));

    // for reduce operation
    double *arrBlocksH;
    double *arrBlocksD;

    // multiply x2 for storing 2 values (block11,block12,...block1k,block21,block22,..block2k)
    CUDA_CHECK_ERR(cudaMallocHost((void**)&arrBlocksH, 2*cudaGridSizeFull*sizeof(double)));
    CUDA_CHECK_ERR(cudaMalloc((void**)&arrBlocksD, 2*cudaGridSizeFull*sizeof(double)));

    startTime = MPI_Wtime();
    for (counterSDI = 1; counterSDI <= SDINum; counterSDI++)
    {
        // The residual vector r(k) = Ax(k)-f is calculating ...
        for (j = 0; j < patchSzY; j++) {
            for (i = 0; i < patchSzX; i++) {
                int idx = patchSzXExt*(j+1) + i + 1;
                ResVect[idx] = LeftPart(SolVect, i + 1, j + 1, patchSzXExt, h0, h1) - RHS_Vect[idx];
            }
        }

        // The value of product (r(k),r(k)) is calculating ...
        sp_local = 0.0;
        for (j = 0; j < patchSzY; j++) {
            for (i = 0; i < patchSzX; i++) {
                int idx = patchSzXExt*(j+1) + i + 1;
                sp_local += ResVect[idx] * ResVect[idx] * h0 * h1;
            }
        }

        // Reduce some for all processes
        MPI_Allreduce(&sp_local, &sp, 1, MPI_DOUBLE, MPI_SUM, Grid_Comm);
        tau = sp;

        // Update ResVect region borders as it is needed for computing Ar(k)
        Communicate(Grid_Comm, ResVect, TAG_RES, sendBuff0, recvBuff0, sendBuff1,
            recvBuff1, patchSzXExt, patchSzYExt, up, down, left, right);

        // The value of product sp = (Ar(k),r(k)) is calculating ...
        sp_local = 0.0;
        for (j = 0; j < patchSzY; j++) {
            for (i = 0; i < patchSzX; i++) {
                int idx = patchSzXExt*(j+1) + i + 1;
                sp_local += LeftPart(ResVect, i + 1, j + 1, patchSzXExt, h0, h1) * ResVect[idx] * h0 * h1;
            }
        }

        // Reduce some for all processes
        MPI_Allreduce(&sp_local, &sp, 1, MPI_DOUBLE, MPI_SUM, Grid_Comm);
        tau = tau / sp;

        // The x(k+1) is calculating ...
        err_local = 0.0;
        for (j = 0; j < patchSzY; j++) {
            for (i = 0; i < patchSzX; i++) {
                int idx = patchSzXExt*(j+1) + i + 1;
                NewValue = SolVect[idx] - tau*ResVect[idx];
                err_local = Max(err_local, fabs(NewValue - SolVect[idx]));
                SolVect[idx] = NewValue;
            }
        }

        MPI_Allreduce(&err_local, &err, 1, MPI_DOUBLE, MPI_MAX, Grid_Comm);

        // Update solution on region borders
        Communicate(Grid_Comm, SolVect, TAG_SOLV, sendBuff0, recvBuff0, sendBuff1,
            recvBuff1, patchSzXExt, patchSzYExt, up, down, left, right);

#ifdef Print
        if (rank == 0 && (counterSDI % PrintStep == 0)) {
            printf("SDI iteration %d, delta=%f\n", counterSDI, err);
        }
#endif
    }
    counterSDI--;
    endTime = MPI_Wtime();
    totalTime += (endTime - startTime);
    // the end of steep descent iteration.

    if (rank == 0) {
        printf("SDI iterations finished, number of SDI iterations = %d, last delta = %f\n", counterSDI, err);
        printf("Running CGM iterations...\n");
    }

    // copy data to device
    // g(0) = r(k-1).
    CUDA_CHECK_ERR(cudaMemcpy(BasisVectD, ResVect, patchSzXExt*patchSzYExt*sizeof(double), cudaMemcpyHostToDevice));
    // current solution
    CUDA_CHECK_ERR(cudaMemcpy(SolVectD, SolVect, patchSzXExt*patchSzYExt*sizeof(double), cudaMemcpyHostToDevice));
    // right part
    CUDA_CHECK_ERR(cudaMemcpy(RHS_VectD, RHS_Vect, patchSzXExt*patchSzYExt*sizeof(double), cudaMemcpyHostToDevice));
    // zero residual
    CUDA_CHECK_ERR(cudaMemset(ResVectD, 0, patchSzXExt*patchSzYExt*sizeof(double)));

    // CGM iterations begin ...
    // sp == (Ar(k-1),r(k-1)) == (Ag(0),g(0)), k=1.
    startTime = MPI_Wtime();
    for (counterCGM = 1; counterCGM <= CGMNum; counterCGM++)
    {
        iterStart = MPI_Wtime();
        // --------------------------------------------------------------------------------
        // The residual vector r(k)
        // call kernel
        cudaNewResidualKernel<<<dimGrid, dimBlock>>>(ResVectD, SolVectD, RHS_VectD,
                    patchSzXExt, h0, h1,
                    patchSzX, patchSzY);

        CUDA_CHECK_ERR(cudaPeekAtLastError());
        CUDA_CHECK_ERR(cudaDeviceSynchronize());

        // Update ResVect region borders as it is needed for computing Ar(k)
        Communicate(Grid_Comm, ResVectD, TAG_RES, sendBuff0, recvBuff0, sendBuff1,
            recvBuff1, patchSzXExt, patchSzYExt, up, down, left, right);

        // --------------------------------------------------------------------------------
        // The value of product (Ar(k),g(k-1))
        // call kernel
        productArrKernel<<<dimGrid, dimBlock>>>(ResVectD, BasisVectD, arrBlocksD,
                    patchSzXExt, h0, h1, patchSzX, patchSzY);

        CUDA_CHECK_ERR(cudaPeekAtLastError());
        CUDA_CHECK_ERR(cudaDeviceSynchronize());

        // send to host
        CUDA_CHECK_ERR(cudaMemcpy(arrBlocksH, arrBlocksD, sizeof(double)*cudaGridSizeFull, cudaMemcpyDeviceToHost));
        alpha_local = 0.0;
        for (int k_block = 0; k_block < cudaGridSizeFull; k_block++) {
            alpha_local += arrBlocksH[k_block];
        }

        // Reduce some for all processes
        MPI_Allreduce(&alpha_local, &alpha, 1, MPI_DOUBLE, MPI_SUM, Grid_Comm);
        alpha = alpha / sp;

        // --------------------------------------------------------------------------------
        // The new basis vector g(k)
        // call kernel
        cudaNewBasisKernel<<<dimGrid, dimBlock>>>(ResVectD, BasisVectD, alpha,
                    patchSzXExt, patchSzX, patchSzY);
        CUDA_CHECK_ERR(cudaPeekAtLastError());
        CUDA_CHECK_ERR(cudaDeviceSynchronize());

        // --------------------------------------------------------------------------------
        // The value of product (r(k),g(k))
        // call kernel
        productRGKernel<<<dimGrid, dimBlock>>>(ResVectD, BasisVectD, arrBlocksD,
                    patchSzXExt, h0*h1, patchSzX, patchSzY);
        CUDA_CHECK_ERR(cudaPeekAtLastError());
        CUDA_CHECK_ERR(cudaDeviceSynchronize());

        // send to host
        CUDA_CHECK_ERR(cudaMemcpy(arrBlocksH, arrBlocksD, cudaGridSizeFull*sizeof(double), cudaMemcpyDeviceToHost));
        tau_local = 0.0;
        for (int k_block = 0; k_block < cudaGridSizeFull; k_block++) {
            tau_local += arrBlocksH[k_block];
        }

        // Reduce some for all processes
        MPI_Allreduce(&tau_local, &tau, 1, MPI_DOUBLE, MPI_SUM, Grid_Comm);

        // Update BasisVect region borders as it is needed for computing Ag(k)
        Communicate(Grid_Comm, BasisVectD, TAG_BAS, sendBuff0, recvBuff0, sendBuff1,
            recvBuff1, patchSzXExt, patchSzYExt, up, down, left, right);

        // --------------------------------------------------------------------------------
        // The value of product sp = (Ag(k),g(k))
        // call kernel
        productArrKernel<<<dimGrid, dimBlock>>>(BasisVectD, BasisVectD, arrBlocksD,
                    patchSzXExt, h0, h1, patchSzX, patchSzY);

        CUDA_CHECK_ERR(cudaPeekAtLastError());
        CUDA_CHECK_ERR(cudaDeviceSynchronize());

        // send to host
        CUDA_CHECK_ERR(cudaMemcpy(arrBlocksH, arrBlocksD, cudaGridSizeFull*sizeof(double), cudaMemcpyDeviceToHost));
        sp_local = 0.0;
        for (int k_block = 0; k_block < cudaGridSizeFull; k_block++) {
            sp_local += arrBlocksH[k_block];
        }

        // Reduce some for all processes
        MPI_Allreduce(&sp_local, &sp, 1, MPI_DOUBLE, MPI_SUM, Grid_Comm);
        tau = tau / sp;

        // --------------------------------------------------------------------------------
        // The x(k+1)
        // update solution and compute error
        // call kernel
        updateSolutionAndGetErrorKernel<<<dimGrid, dimBlock>>>(SolVectD, BasisVectD, arrBlocksD, tau,
                    st0, st1, patchSzXExt, h0, h1, patchSzX, patchSzY);

        CUDA_CHECK_ERR(cudaPeekAtLastError());
        CUDA_CHECK_ERR(cudaDeviceSynchronize());

        // send to host (2x values for delta error and real error)
        CUDA_CHECK_ERR(cudaMemcpy(arrBlocksH, arrBlocksD, 2*cudaGridSizeFull*sizeof(double), cudaMemcpyDeviceToHost));

        err_local = 0.0;
        real_err_local = 0.0;
        for (int k_block = 0; k_block < cudaGridSizeFull; k_block++) {
            err_local = Max(err_local, arrBlocksH[k_block]);
            real_err_local = Max(real_err_local, arrBlocksH[k_block + cudaGridSizeFull]);
        }

        MPI_Allreduce(&err_local, &err, 1, MPI_DOUBLE, MPI_MAX, Grid_Comm);
        MPI_Allreduce(&real_err_local, &real_err, 1, MPI_DOUBLE, MPI_MAX, Grid_Comm);

        Communicate(Grid_Comm, SolVectD, TAG_SOLV, sendBuff0, recvBuff0, sendBuff1, recvBuff1,
            patchSzXExt, patchSzYExt, up, down, left, right);

        iterEnd = MPI_Wtime();
        iterTime = 1000 * (iterEnd - iterStart); // in msec

#ifdef Print
        if (rank == 0 && (counterCGM % PrintStep == 0)) {
            printf("CGM iteration %d, time=%.2f, delta=%f, real error=%f\n", counterCGM, iterTime, err, real_err);
            fprintf(fp_iter, "%d %f\n", counterCGM, real_err);
        }
#endif

        if (err < MIN_ERR) {
            counterCGM++;
            break;
        }
    }
    counterCGM--;
    endTime = MPI_Wtime();
    totalTime += (endTime - startTime);
    iterAvgTime = (endTime - startTime) / ((double)(counterCGM));

    // the end of CGM iterations.
    if (rank == 0) {
        fclose(fp_iter);
        printf("CGM iterations finished, number of CGM iterations = %d, last delta = %f\n", counterCGM, err);
        printf("Gathering results...\n");
    }

    // Gather results

    // send solution to host
    startTime = MPI_Wtime();

    CUDA_CHECK_ERR(cudaMemcpy(SolVect, SolVectD, patchSzXExt*patchSzYExt*sizeof(double), cudaMemcpyDeviceToHost));

    // send borders for patches near edges
    int _st0 = is_edge_st0 ? 0 : st0;
    int _en0 = is_edge_en0 ? N0 : en0;
    int _st1 = is_edge_st1 ? 0 : st1;
    int _en1 = is_edge_en1 ? N1 : en1;

    int _srcOffset0 = st0 - _st0; // zero or one
    int _srcOffset1 = st1 - _st1; // zero or one

    // (n0 + 1) is the maximum size of patch along axis 0, same for axis 1: (n1 + 1)
    int sendCount = 4 + (n0 + 1)*(n1 + 1); // <st0><en0><st1><en1><actual data>[<empty>]
    double *sendData;
    CUDA_CHECK_ERR(cudaMallocHost((void**)&sendData, sendCount*sizeof(double)));

    // prepare send
    sendData[0] = (double)(_st0);
    sendData[1] = (double)(_en0);
    sendData[2] = (double)(_st1);
    sendData[3] = (double)(_en1);
    for (j = 0; j < _en1 - _st1; j++) {
        for (i = 0; i < _en0 - _st0; i++) {
            int dstIdx = 4 + (n0 + 1)*j + i;
            int srcIdx = patchSzXExt*(j + 1 - _srcOffset1) + (i + 1 - _srcOffset0);
            sendData[dstIdx] = SolVect[srcIdx];
        }
    }

    // receive
    int recvCount = sendCount;
    double *recvData = NULL;
    double *SolVectFull = NULL;
    if (rank == 0) {
        CUDA_CHECK_ERR(cudaMallocHost((void**)&recvData, ProcNum*recvCount*sizeof(double)));
        CUDA_CHECK_ERR(cudaMallocHost((void**)&SolVectFull, N0*N1*sizeof(double)));
    }
    MPI_Gather(sendData, sendCount, MPI_DOUBLE, recvData, recvCount, MPI_DOUBLE, 0, Grid_Comm);

    endTime = MPI_Wtime();
    totalTime += (endTime - startTime);

    // merge in process 0
    if (rank == 0)
    {
        double *curData = recvData;
        for (int k = 0; k < ProcNum; k++)
        {
            _st0 = (int)round(curData[0]);
            _en0 = (int)round(curData[1]);
            _st1 = (int)round(curData[2]);
            _en1 = (int)round(curData[3]);

            for (int j = 0; j < _en1 - _st1; j++)
            {
                for (int i = 0; i < _en0 - _st0; i++)
                {
                    SolVectFull[N0*(j + _st1) + i + _st0] = curData[4 + (n0 + 1)*j + i];
                }
            }

            curData += recvCount;
        }

        printf("Results gathered.\n");
        printf("Total time: %f sec\n", totalTime);
        printf("Time per CGM iteration: %f msec\n", iterAvgTime * 1000);
        printf("Computing error...\n");

        // compare with the solution
        double maxErr = 0.0;
        double x;
        double y;
        double gtVal;
        double compCalc;
        for (int j = 0; j < N1; j++)
        {
            y = ypos(j, h1);
            for (int i = 0; i < N0; i++)
            {
                x = xpos(i, h0);
                compCalc = SolVectFull[N0 * j + i];
                gtVal = Solution(x, y);
                maxErr = Max(maxErr, fabs(compCalc - gtVal));
            }
        }
        compErr = maxErr;
        printf("Real error = %f\n", compErr);

        printf("Writing results...\n");
        // printing results ...
        fprintf(fp, "The steep descent iterations number: %d\nThe conjugate gradient iterations number: %d\n", counterSDI, counterCGM);
        fprintf(fp, "The %d iterations are carried out. Time for computing is %f sec. Time per CGM iteration is %.4f msec. The real error is %.12f.\n",
            counterSDI + counterCGM, totalTime, iterAvgTime * 1000, compErr);
        fclose(fp);

#if DoOutputRes
        sprintf(str, "Puasson_CUDA_ECGM_%dx%d_%d.dat", N0, N1, ProcNum);
        fp = fopen(str, "w");
        fprintf(fp, "# This is the conjugate gradient method for descrete Puasson equation.\n"
            "# A = %f, B = %f, N[0,A] = %d, N[0,B] = %d, SDINum = %d, CGMNum = %d.\n"
            "# One can draw it by gnuplot by the command: splot 'MyPath\\FileName.dat' with lines\n", \
            A, B, N0, N1, counterSDI, counterCGM);
        for (int j = 0; j < N1; j++)
        {
            for (int i = 0; i < N0; i++)
                fprintf(fp, "\n%f %f %f", xpos(i, h0), ypos(j, h1), SolVectFull[N0*j + i]);
            fprintf(fp, "\n");
        }
        fclose(fp);
#endif
    }

    if (rank == 0) {
        printf("Finihsed writing.\n");
        printf("Finalize program...\n");
    }

    CUDA_CHECK_ERR(cudaFree(arrBlocksD));
    CUDA_CHECK_ERR(cudaFreeHost(arrBlocksH));

    CUDA_CHECK_ERR(cudaFreeHost(sendData));
    if (SolVectFull != NULL) {
        CUDA_CHECK_ERR(cudaFreeHost(SolVectFull));
    }
    if (recvData != NULL) {
        CUDA_CHECK_ERR(cudaFreeHost(recvData));
    }

    CUDA_CHECK_ERR(cudaFreeHost(sendBuff0));
    CUDA_CHECK_ERR(cudaFreeHost(recvBuff0));
    CUDA_CHECK_ERR(cudaFreeHost(sendBuff1));
    CUDA_CHECK_ERR(cudaFreeHost(recvBuff1));

    CUDA_CHECK_ERR(cudaFree(SolVectD));
    CUDA_CHECK_ERR(cudaFree(ResVectD));
    CUDA_CHECK_ERR(cudaFree(BasisVectD));
    CUDA_CHECK_ERR(cudaFree(RHS_VectD));

    CUDA_CHECK_ERR(cudaFreeHost(SolVect));
    CUDA_CHECK_ERR(cudaFreeHost(ResVect));
    CUDA_CHECK_ERR(cudaFreeHost(RHS_Vect));

    MPI_Finalize();

    if (rank == 0) {
        printf("All done.\n");
    }

    return(0);
}
