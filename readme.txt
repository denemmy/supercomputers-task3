1) Компиляция

Предварительные команды:
ssh compiler
module add cuda
module add impi

Скомпилировать MPI + CUDA:
make
или
make cuda

Скомпилировать только MPI-версию:
make mpi

2) Запуск

Примеры запусков есть в файлах run_cuda.sh и run_mpi.sh