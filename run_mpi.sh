# 1000 x 1000
sbatch -p gputest -n16 -t 0-00:05:00 --ntasks-per-node 2 impi ./Puasson_MPI 1000 1000 1 100000
sbatch -p gputest -n8 -t 0-00:05:00 --ntasks-per-node 1 impi ./Puasson_MPI 1000 1000 1 100000
sbatch -p gputest -n4 -t 0-00:05:00 --ntasks-per-node 1 impi ./Puasson_MPI 1000 1000 1 100000
sbatch -p gputest -n2 -t 0-00:10:00 --ntasks-per-node 1 impi ./Puasson_MPI 1000 1000 1 100000

# 2000 x 2000
sbatch -p gputest -n16 -t 0-00:10:00 --ntasks-per-node 2 impi ./Puasson_MPI 2000 2000 1 100000
sbatch -p gputest -n8 -t 0-00:10:00 --ntasks-per-node 1 impi ./Puasson_MPI 2000 2000 1 100000
sbatch -p gputest -n4 -t 0-00:10:00 --ntasks-per-node 1 impi ./Puasson_MPI 2000 2000 1 100000
sbatch -p gputest -n2 -t 0-00:15:00 --ntasks-per-node 1 impi ./Puasson_MPI 2000 2000 1 100000

# test
sbatch -p gputest -n1 -t 0-00:05:00 --ntasks-per-node 1 impi ./Puasson_MPI 100 100 1 100