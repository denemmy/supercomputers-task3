all: cuda

cuda: Puasson_MPI_cuda.cu
	nvcc -arch=compute_20 -code=sm_20 -ccbin mpicxx Puasson_MPI_cuda.cu -o Puasson_MPI_cuda -Xcompiler -Wall -O3

mpi: Puasson_MPI.c
	mpicxx Puasson_MPI.c -o Puasson_MPI -Wall -O3

clean:
	rm -rf *o Puasson_MPI_cuda Puasson_MPI